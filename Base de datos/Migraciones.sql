-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 12-05-2021 a las 01:09:11
-- Versión del servidor: 8.0.13-4
-- Versión de PHP: 7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `VY6mp1acii`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Administrators`
--

CREATE TABLE `Administrators` (
  `Id` bigint(20) NOT NULL,
  `FirebaseId` text COLLATE utf8_unicode_ci NOT NULL,
  `Name` varchar(63) COLLATE utf8_unicode_ci NOT NULL,
  `Email` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Clients`
--

CREATE TABLE `Clients` (
  `Id` bigint(20) NOT NULL,
  `Name` varchar(63) COLLATE utf8_unicode_ci NOT NULL,
  `Phone` text COLLATE utf8_unicode_ci NOT NULL,
  `Email` text COLLATE utf8_unicode_ci NOT NULL,
  `Diseases` text COLLATE utf8_unicode_ci,
  `Medicines` text COLLATE utf8_unicode_ci,
  `EmergencyContacts` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `InstructorRoom`
--

CREATE TABLE `InstructorRoom` (
  `InstructorsId` bigint(20) NOT NULL,
  `RoomsId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Instructors`
--

CREATE TABLE `Instructors` (
  `Id` bigint(20) NOT NULL,
  `Name` varchar(63) COLLATE utf8_unicode_ci NOT NULL,
  `Phone` text COLLATE utf8_unicode_ci NOT NULL,
  `Email` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `InstructorSpeciality`
--

CREATE TABLE `InstructorSpeciality` (
  `InstructorsId` bigint(20) NOT NULL,
  `SpecialitiesId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Reservations`
--

CREATE TABLE `Reservations` (
  `Id` bigint(20) NOT NULL,
  `ClientId` bigint(20) NOT NULL,
  `SessionId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ReservationSession`
--

CREATE TABLE `ReservationSession` (
  `ReservationsId` bigint(20) NOT NULL,
  `SessionsId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Rooms`
--

CREATE TABLE `Rooms` (
  `Id` bigint(20) NOT NULL,
  `Name` varchar(63) COLLATE utf8_unicode_ci NOT NULL,
  `MaxCapacity` int(11) NOT NULL,
  `MaxCapacityAllowed` float NOT NULL,
  `AdministratorId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `RoomSchedule`
--

CREATE TABLE `RoomSchedule` (
  `RoomsId` bigint(20) NOT NULL,
  `SchedulesId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `RoomSpeciality`
--

CREATE TABLE `RoomSpeciality` (
  `RoomsId` bigint(20) NOT NULL,
  `SpecialitiesId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Schedules`
--

CREATE TABLE `Schedules` (
  `Id` bigint(20) NOT NULL,
  `Year` int(11) NOT NULL,
  `Month` int(11) NOT NULL,
  `Day` int(11) NOT NULL,
  `OpensAt` timestamp NOT NULL,
  `ClosesAt` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ScheduleSession`
--

CREATE TABLE `ScheduleSession` (
  `SchedulesId` bigint(20) NOT NULL,
  `SessionsId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SessionCompetitors`
--

CREATE TABLE `SessionCompetitors` (
  `Id` bigint(20) NOT NULL,
  `PaidAt` timestamp NULL DEFAULT NULL,
  `EnterAt` timestamp NOT NULL,
  `LeaveAt` timestamp NOT NULL,
  `SessionHistoryId` bigint(20) NOT NULL,
  `ClientId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SessionHistories`
--

CREATE TABLE `SessionHistories` (
  `Id` bigint(20) NOT NULL,
  `Date` timestamp NOT NULL,
  `StartedAt` timestamp NOT NULL,
  `EndedAt` timestamp NOT NULL,
  `SessionId` bigint(20) NOT NULL,
  `InstructorId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Sessions`
--

CREATE TABLE `Sessions` (
  `Id` bigint(20) NOT NULL,
  `InstructorId` bigint(20) NOT NULL,
  `Duration` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SessionSpeciality`
--

CREATE TABLE `SessionSpeciality` (
  `SessionsId` bigint(20) NOT NULL,
  `SpecialitiesId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Specialities`
--

CREATE TABLE `Specialities` (
  `Id` bigint(20) NOT NULL,
  `Name` varchar(63) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `__EFMigrationsHistory`
--

CREATE TABLE `__EFMigrationsHistory` (
  `MigrationId` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `ProductVersion` varchar(32) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `__EFMigrationsHistory`
--

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`) VALUES
('20210512010235_Initial', '5.0.6');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Administrators`
--
ALTER TABLE `Administrators`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `Clients`
--
ALTER TABLE `Clients`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `InstructorRoom`
--
ALTER TABLE `InstructorRoom`
  ADD PRIMARY KEY (`InstructorsId`,`RoomsId`),
  ADD KEY `IX_InstructorRoom_RoomsId` (`RoomsId`);

--
-- Indices de la tabla `Instructors`
--
ALTER TABLE `Instructors`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `InstructorSpeciality`
--
ALTER TABLE `InstructorSpeciality`
  ADD PRIMARY KEY (`InstructorsId`,`SpecialitiesId`),
  ADD KEY `IX_InstructorSpeciality_SpecialitiesId` (`SpecialitiesId`);

--
-- Indices de la tabla `Reservations`
--
ALTER TABLE `Reservations`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_Reservations_ClientId` (`ClientId`);

--
-- Indices de la tabla `ReservationSession`
--
ALTER TABLE `ReservationSession`
  ADD PRIMARY KEY (`ReservationsId`,`SessionsId`),
  ADD KEY `IX_ReservationSession_SessionsId` (`SessionsId`);

--
-- Indices de la tabla `Rooms`
--
ALTER TABLE `Rooms`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_Rooms_AdministratorId` (`AdministratorId`);

--
-- Indices de la tabla `RoomSchedule`
--
ALTER TABLE `RoomSchedule`
  ADD PRIMARY KEY (`RoomsId`,`SchedulesId`),
  ADD KEY `IX_RoomSchedule_SchedulesId` (`SchedulesId`);

--
-- Indices de la tabla `RoomSpeciality`
--
ALTER TABLE `RoomSpeciality`
  ADD PRIMARY KEY (`RoomsId`,`SpecialitiesId`),
  ADD KEY `IX_RoomSpeciality_SpecialitiesId` (`SpecialitiesId`);

--
-- Indices de la tabla `Schedules`
--
ALTER TABLE `Schedules`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `ScheduleSession`
--
ALTER TABLE `ScheduleSession`
  ADD PRIMARY KEY (`SchedulesId`,`SessionsId`),
  ADD KEY `IX_ScheduleSession_SessionsId` (`SessionsId`);

--
-- Indices de la tabla `SessionCompetitors`
--
ALTER TABLE `SessionCompetitors`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_SessionCompetitors_ClientId` (`ClientId`),
  ADD KEY `IX_SessionCompetitors_SessionHistoryId` (`SessionHistoryId`);

--
-- Indices de la tabla `SessionHistories`
--
ALTER TABLE `SessionHistories`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_SessionHistories_InstructorId` (`InstructorId`),
  ADD KEY `IX_SessionHistories_SessionId` (`SessionId`);

--
-- Indices de la tabla `Sessions`
--
ALTER TABLE `Sessions`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_Sessions_InstructorId` (`InstructorId`);

--
-- Indices de la tabla `SessionSpeciality`
--
ALTER TABLE `SessionSpeciality`
  ADD PRIMARY KEY (`SessionsId`,`SpecialitiesId`),
  ADD KEY `IX_SessionSpeciality_SpecialitiesId` (`SpecialitiesId`);

--
-- Indices de la tabla `Specialities`
--
ALTER TABLE `Specialities`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `__EFMigrationsHistory`
--
ALTER TABLE `__EFMigrationsHistory`
  ADD PRIMARY KEY (`MigrationId`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Administrators`
--
ALTER TABLE `Administrators`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Clients`
--
ALTER TABLE `Clients`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Instructors`
--
ALTER TABLE `Instructors`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Reservations`
--
ALTER TABLE `Reservations`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Rooms`
--
ALTER TABLE `Rooms`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Schedules`
--
ALTER TABLE `Schedules`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `SessionCompetitors`
--
ALTER TABLE `SessionCompetitors`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `SessionHistories`
--
ALTER TABLE `SessionHistories`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Sessions`
--
ALTER TABLE `Sessions`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Specialities`
--
ALTER TABLE `Specialities`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `InstructorRoom`
--
ALTER TABLE `InstructorRoom`
  ADD CONSTRAINT `FK_InstructorRoom_Instructors_InstructorsId` FOREIGN KEY (`InstructorsId`) REFERENCES `Instructors` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_InstructorRoom_Rooms_RoomsId` FOREIGN KEY (`RoomsId`) REFERENCES `Rooms` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `InstructorSpeciality`
--
ALTER TABLE `InstructorSpeciality`
  ADD CONSTRAINT `FK_InstructorSpeciality_Instructors_InstructorsId` FOREIGN KEY (`InstructorsId`) REFERENCES `Instructors` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_InstructorSpeciality_Specialities_SpecialitiesId` FOREIGN KEY (`SpecialitiesId`) REFERENCES `Specialities` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `Reservations`
--
ALTER TABLE `Reservations`
  ADD CONSTRAINT `FK_Reservations_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `Clients` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `ReservationSession`
--
ALTER TABLE `ReservationSession`
  ADD CONSTRAINT `FK_ReservationSession_Reservations_ReservationsId` FOREIGN KEY (`ReservationsId`) REFERENCES `Reservations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_ReservationSession_Sessions_SessionsId` FOREIGN KEY (`SessionsId`) REFERENCES `Sessions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `Rooms`
--
ALTER TABLE `Rooms`
  ADD CONSTRAINT `FK_Rooms_Administrators_AdministratorId` FOREIGN KEY (`AdministratorId`) REFERENCES `Administrators` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `RoomSchedule`
--
ALTER TABLE `RoomSchedule`
  ADD CONSTRAINT `FK_RoomSchedule_Rooms_RoomsId` FOREIGN KEY (`RoomsId`) REFERENCES `Rooms` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_RoomSchedule_Schedules_SchedulesId` FOREIGN KEY (`SchedulesId`) REFERENCES `Schedules` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `RoomSpeciality`
--
ALTER TABLE `RoomSpeciality`
  ADD CONSTRAINT `FK_RoomSpeciality_Rooms_RoomsId` FOREIGN KEY (`RoomsId`) REFERENCES `Rooms` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_RoomSpeciality_Specialities_SpecialitiesId` FOREIGN KEY (`SpecialitiesId`) REFERENCES `Specialities` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `ScheduleSession`
--
ALTER TABLE `ScheduleSession`
  ADD CONSTRAINT `FK_ScheduleSession_Schedules_SchedulesId` FOREIGN KEY (`SchedulesId`) REFERENCES `Schedules` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_ScheduleSession_Sessions_SessionsId` FOREIGN KEY (`SessionsId`) REFERENCES `Sessions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `SessionCompetitors`
--
ALTER TABLE `SessionCompetitors`
  ADD CONSTRAINT `FK_SessionCompetitors_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `Clients` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_SessionCompetitors_SessionHistories_SessionHistoryId` FOREIGN KEY (`SessionHistoryId`) REFERENCES `SessionHistories` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `SessionHistories`
--
ALTER TABLE `SessionHistories`
  ADD CONSTRAINT `FK_SessionHistories_Instructors_InstructorId` FOREIGN KEY (`InstructorId`) REFERENCES `Instructors` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_SessionHistories_Sessions_SessionId` FOREIGN KEY (`SessionId`) REFERENCES `Sessions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `Sessions`
--
ALTER TABLE `Sessions`
  ADD CONSTRAINT `FK_Sessions_Instructors_InstructorId` FOREIGN KEY (`InstructorId`) REFERENCES `Instructors` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `SessionSpeciality`
--
ALTER TABLE `SessionSpeciality`
  ADD CONSTRAINT `FK_SessionSpeciality_Sessions_SessionsId` FOREIGN KEY (`SessionsId`) REFERENCES `Sessions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_SessionSpeciality_Specialities_SpecialitiesId` FOREIGN KEY (`SpecialitiesId`) REFERENCES `Specialities` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
