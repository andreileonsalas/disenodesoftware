using GraphQL.Server.Ui.Voyager;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Proyecto1.API.Database;
using Proyecto1.API.GraphQL.Queries;

namespace Proyecto1.API
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options => options.AddDefaultPolicy(
                policy => policy.AllowAnyMethod().AllowAnyOrigin().AllowAnyHeader()
            ));

            services.AddPooledDbContextFactory<ApplicationDbContext>(options => options.UseMySQL(_configuration.GetConnectionString("MySql")));

            services
                .AddGraphQLServer()
                .AddQueryType<RootQuery>()
                .AddProjections();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbContextFactory<ApplicationDbContext> dbContextFactory)
        {
            using var context = dbContextFactory.CreateDbContext();
            new Seed(context).SeedDatabase();

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseCors();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGraphQL();
            });

            app.UseGraphQLVoyager("/voyager");
        }
    }
}
