﻿using Microsoft.EntityFrameworkCore;
using Proyecto1.API.Models;

namespace Proyecto1.API.Database
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Instructor> Instructors { get; set; }
        public DbSet<Speciality> Specialities { get; set; }
        public DbSet<Administrator> Administrators { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<SessionHistory> SessionHistories { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<SessionCompetitor> SessionCompetitors { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
    }
}
