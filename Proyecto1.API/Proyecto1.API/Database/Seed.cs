﻿using Bogus;
using Microsoft.EntityFrameworkCore;
using Proyecto1.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto1.API.Database
{
    public class Seed
    {
        private readonly Faker _faker = new("es");
        private readonly Random _random = new();
        private readonly ApplicationDbContext _context;

        public Seed(ApplicationDbContext context)
        {
            _context = context;
        }

        public void SeedDatabase()
        {
            if (!_context.Administrators.Any())
            {
                _context.Administrators.AddRange(SeedAdministrators());
                _context.SaveChanges();
            }

            if (!_context.Specialities.Any())
            {
                _context.Specialities.AddRange(SeedSpecialities());
                _context.SaveChanges();
            }

            if (!_context.Instructors.Any())
            {
                _context.Instructors.AddRange(SeedInstructors());
                _context.SaveChanges();
            }

            if (!_context.Clients.Any())
            {
                _context.Clients.AddRange(SeedClients());
                _context.SaveChanges();
            }

            if (!_context.Rooms.Any())
            {
                _context.Rooms.AddRange(SeedRooms());
                _context.SaveChanges();
            }

            if (!_context.Sessions.Any())
            {
                _context.Sessions.AddRange(SeedSessions());
                _context.SaveChanges();
            }

            if (!_context.Reservations.Any())
            {
                _context.Reservations.AddRange(SeedReservation());
                _context.SaveChanges();
            }
        }

        private IEnumerable<Administrator> SeedAdministrators()
        {
            for (int i = 0; i < 5; i++)
                yield return new Administrator
                {
                    Name = _faker.Name.FullName(),
                    Email = _faker.Internet.Email(),
                    FirebaseId = Guid.NewGuid().ToString("N"),
                };
        }

        private IEnumerable<Speciality> SeedSpecialities()
        {
            var specialities = new[]
            {
                "Acondicionamiento Físico",
                "Entrenamiento Funcional",
                "Crossfit",
                "Cardio-Dance",
                "Entrenamiento Personalizado",
                "Yoga",
                "Acrotelas",
                "Boxing",
            };

            foreach (var item in specialities)
                yield return new Speciality
                {
                    Name = item,
                };
        }

        private IEnumerable<Instructor> SeedInstructors()
        {
            var specialities = _context.Specialities.ToList();
            for (int i = 0; i < 15; i++)
            {
                yield return new Instructor
                {
                    Name = _faker.Name.FullName(),
                    Email = _faker.Internet.Email(),
                    Phone = _faker.Phone.PhoneNumber(),
                    Specialities = PickSome(specialities)
                };
            }
        }

        private IEnumerable<Client> SeedClients()
        {
            var diseases = new[] { "Lepra", "Sida", "Asma", "Sífilis", "Herpes", "COVID 19", "Intolerante a la lactosa", "Cáncer pulmonal" }.ToList();
            var medicines = new[] { "Paracetamol", "Simvastatina", "Aspirina", "Omeprazol", "Lexotiroxina sódica", "Ramipril" }.ToList();
            var emergency = Enumerable.Range(0, 50).Select(_ => _faker.Phone.PhoneNumber()).ToList();
            for (int i = 0; i < 30; i++)
            {
                yield return new Client
                {
                    Name = _faker.Name.FullName(),
                    Email = _faker.Internet.Email(),
                    Phone = _faker.Phone.PhoneNumber(),
                    Diseases = PickSome(diseases).Aggregate((a, b) => $"{a}, {b}"),
                    EmergencyContacts = PickSome(emergency).Aggregate((a, b) => $"{a}, {b}"),
                    Medicines = PickSome(medicines).Aggregate((a, b) => $"{a}, {b}"),
                };
            }
        }

        private IEnumerable<Room> SeedRooms()
        {
            var instructors = _context.Instructors.ToList();
            var administrator = _context.Administrators.ToList();
            var specialities = _context.Specialities.ToList();
            for (int i = 0; i < 7; i++)
            {
                yield return new Room
                {
                    Name = _faker.Name.FullName(),
                    MaxCapacity = _random.Next(10, 50),
                    MaxCapacityAllowed = _random.Next(20, 80),
                    Specialities = PickSome(specialities),
                    Instructors = PickSome(instructors),
                    Administrator = Pick(administrator),
                    Schedules = SeedSchedules().ToList(),
                };
            }
        }

        private IEnumerable<Session> SeedSessions()
        {
            var instructors = _context.Instructors.ToList();
            var rooms = _context.Rooms.ToList();
            var specialities = _context.Specialities.ToList();
            var clients = _context.Clients.ToList();
            for (int i = 0; i < 14; i++)
            {
                yield return new Session
                {
                    Duration = _random.Next(1, 6),
                    Instructor = Pick(instructors),
                    Schedules = SeedSchedules().ToList(),
                    Specialities = PickSome(specialities),
                    SessionHistories = SeedHistory(instructors, clients).ToList(),
                    Room = Pick(rooms)
                };
            }
        }

        private IEnumerable<Reservation> SeedReservation()
        {
            var sessions = _context.Sessions.ToList();
            var clients = _context.Clients.ToList();
            for (int i = 0; i < 25; i++)
            {
                yield return new Reservation
                {
                    Client = Pick(clients),
                    Sessions = PickSome(sessions)
                };
            }
        }


        private IEnumerable<SessionHistory> SeedHistory(List<Instructor> instructors, List<Client> clients)
        {
            for (int i = 0; i < 15; i++)
            {
                var opens = _faker.Date.Recent();
                yield return new SessionHistory
                {
                    Instructor = Pick(instructors),
                    Date = opens,
                    StartedAt = opens,
                    EndedAt = opens.AddHours(_random.Next(1, 4)),
                    SessionCompetitors = SeedCompetitors(clients).ToList()
                };
            }
        }

        private IEnumerable<SessionCompetitor> SeedCompetitors(List<Client> clients)
        {
            for (int i = 0; i < 15; i++)
            {
                var opens = _faker.Date.Recent();
                yield return new SessionCompetitor
                {
                    Client = Pick(clients),
                    EnterAt = opens,
                    LeaveAt = opens.AddHours(_random.Next(1, 3)),
                    PaidAt = _random.Next(0, 10) > 7 ? null : _faker.Date.Past(),
                };
            }
        }

        private IEnumerable<Schedule> SeedSchedules()
        {
            var days = new[] { "L", "M", "X", "J", "V", "S", "D" }.ToList();
            for (int i = 0; i < 5; i++)
            {
                var opens = _faker.Date.Future();
                yield return new Schedule
                {
                    Year = DateTimeOffset.UtcNow.Year,
                    Month = _random.Next(1, 13),
                    Day = Pick(days),
                    OpensAt = opens,
                    ClosesAt = opens.AddHours(_random.Next(1, 6)),
                };
            }
        }

        private List<T> PickSome<T>(List<T> options)
        {
            var localOptions = options.ToList();
            var count = _random.Next(1, Math.Min(4, options.Count));

            return Enumerable.Range(0, count).Select(_ =>
            {
                var selectedSpeciality = localOptions[_random.Next(0, localOptions.Count)];
                localOptions.Remove(selectedSpeciality);
                return selectedSpeciality;
            }).ToList();
        }

        private T Pick<T>(List<T> options)
        {
            return options[_random.Next(0, options.Count)];
        }
    }
}
