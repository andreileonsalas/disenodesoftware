﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Proyecto1.API.Models
{
    public class Session
    {
        public long Id { get; set; }
        
        [Required(ErrorMessage = "Por favor ingrese un instructor.")]
        public long InstructorId { get; set; }

        [Required(ErrorMessage = "Por favor ingrese una duración.")]
        [Range(0, 24, ErrorMessage = "Por favor ingrese una duración entre 0 y 24 horas.")]
        public float Duration { get; set; }
        
        [Required(ErrorMessage = "Por favor ingrese una sala.")]
        public long RoomId { get; set; }

        public Room Room { get; set; }
        public Instructor Instructor { get; set; }
        public ICollection<Schedule> Schedules { get; set; }
        public ICollection<Reservation> Reservations { get; set; }
        public ICollection<Speciality> Specialities { get; set; }
        public ICollection<SessionHistory> SessionHistories { get; set; }
    }
}
