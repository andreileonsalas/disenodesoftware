﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Proyecto1.API.Models
{
    public class Client
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un nombre.")]
        [StringLength(63, MinimumLength = 3, ErrorMessage = "Por favor ingrese un nombre entre 3 y 63 caracteres.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un número telefónico.")]
        [Phone(ErrorMessage = "Por favor ingrese un número telefónico válido.")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un correo electrónico.")]
        [EmailAddress(ErrorMessage = "Por favor ingrese un correo electrónico válido.")]
        public string Email { get; set; }

        public string Diseases { get; set; }
        public string Medicines { get; set; }
        public string EmergencyContacts { get; set; }

        public ICollection<Reservation> Reservations { get; set; }
        public ICollection<SessionCompetitor> SessionCompetitors { get; set; }
    }
}
