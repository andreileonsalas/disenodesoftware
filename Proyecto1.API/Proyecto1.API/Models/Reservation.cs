﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Proyecto1.API.Models
{
    public class Reservation
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un cliente.")]
        public long ClientId { get; set; }

        [Required(ErrorMessage = "Por favor ingrese una sesión.")]
        public long SessionId { get; set; }

        public ICollection<Session> Sessions { get; set; }
        public Client Client { get; set; }
    }
}
