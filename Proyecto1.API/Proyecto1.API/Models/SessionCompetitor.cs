﻿using System;

namespace Proyecto1.API.Models
{
    public class SessionCompetitor
    {
        public long Id { get; set; }

        public DateTimeOffset? PaidAt { get; set; }
        public DateTimeOffset EnterAt { get; set; }
        public DateTimeOffset LeaveAt { get; set; }

        public long SessionHistoryId { get; set; }
        public long ClientId { get; set; }
        public SessionHistory SessionHistory { get; set; }
        public Client Client { get; set; }
    }
}
