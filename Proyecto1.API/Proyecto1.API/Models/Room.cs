﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Proyecto1.API.Models
{
    public class Room
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un nombre.")]
        [StringLength(63, MinimumLength = 3, ErrorMessage = "Por favor ingrese un nombre entre 3 y 63 caracteres.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Por favor ingrese una capacidad.")]
        public int MaxCapacity { get; set; }

        [Range(0, 100, ErrorMessage = "Por favor ingrese un valor entre 0 y 100.")]
        public float MaxCapacityAllowed { get; set; } = 100;
        
        public float CurrentCapacity => MaxCapacity * MaxCapacityAllowed / 100;

        [Required]
        public long AdministratorId { get; set; }

        public Administrator Administrator { get; set; }
        public ICollection<Speciality> Specialities { get; set; }
        public ICollection<Instructor> Instructors { get; set; }
        public ICollection<Schedule> Schedules { get; set; }
        public ICollection<Session> Sessions { get; set; }
    }
}
