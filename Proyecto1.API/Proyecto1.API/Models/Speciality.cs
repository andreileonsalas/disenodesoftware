﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Proyecto1.API.Models
{
    public class Speciality
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un nombre.")]
        [StringLength(63, MinimumLength = 3, ErrorMessage = "Por favor ingrese un nombre entre 3 y 63 caracteres.")]
        public string Name { get; set; }

        public ICollection<Instructor> Instructors { get; set; }
        public ICollection<Room> Rooms { get; set; }
        public ICollection<Session> Sessions { get; set; }
    }
}
