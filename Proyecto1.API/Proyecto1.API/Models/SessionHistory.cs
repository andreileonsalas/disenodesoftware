﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Proyecto1.API.Models
{
    public class SessionHistory
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Por favor ingrese una fecha.")]
        public DateTimeOffset Date { get; set; }

        [Required(ErrorMessage = "Por favor ingrese una fecha de inicio.")]
        public DateTimeOffset StartedAt { get; set; }

        [Required(ErrorMessage = "Por favor ingrese una fecha de fin.")]
        public DateTimeOffset EndedAt { get; set; }

        [Required(ErrorMessage = "Por favor ingrese una sesión.")]
        public long SessionId { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un instructor.")]
        public long InstructorId { get; set; }
        public Session Session { get; set; }
        public Instructor Instructor { get; set; }
        public ICollection<SessionCompetitor> SessionCompetitors { get; set; }
    }
}
