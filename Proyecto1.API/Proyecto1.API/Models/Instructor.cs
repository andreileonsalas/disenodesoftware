﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Proyecto1.API.Models
{
    public class Instructor
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un nombre.")]
        [StringLength(63, MinimumLength = 3, ErrorMessage = "Por favor ingrese un nombre entre 3 y 63 caracteres.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un número telefónico.")]
        [Phone(ErrorMessage = "Por favor ingrese un número telefónico válido.")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un correo electrónico.")]
        [EmailAddress(ErrorMessage = "Por favor ingrese un correo electrónico válido.")]
        public string Email { get; set; }

        public ICollection<Room> Rooms { get; set; }
        public ICollection<Speciality> Specialities { get; set; }
    }
}
