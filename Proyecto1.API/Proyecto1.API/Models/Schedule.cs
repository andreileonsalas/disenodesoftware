﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Proyecto1.API.Models
{
    public class Schedule
    {
        public long Id { get; set; }

        public int Year { get; set; }

        public int Month { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un día.")]
        [StringLength(1, MinimumLength = 1, ErrorMessage = "Por favor ingrese solo un caracter.")]
        public string Day { get; set; }

        [Required(ErrorMessage = "Por favor ingrese la hora de apertura.")]
        public DateTimeOffset OpensAt { get; set; }

        [Required(ErrorMessage = "Por favor ingrese la hora de cierre.")]
        public DateTimeOffset ClosesAt { get; set; }


        public ICollection<Room> Rooms { get; set; }
        public ICollection<Session> Sessions { get; set; }
    }
}
