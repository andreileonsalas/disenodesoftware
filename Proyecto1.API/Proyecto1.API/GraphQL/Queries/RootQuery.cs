﻿using HotChocolate;
using HotChocolate.Data;
using Proyecto1.API.Database;
using Proyecto1.API.Models;
using System.Linq;

namespace Proyecto1.API.GraphQL.Queries
{
    public class RootQuery
    {
        [UseDbContext(typeof(ApplicationDbContext))]
        [UseProjection]
        public IQueryable<Room> GetRooms([ScopedService] ApplicationDbContext db) => db.Rooms;

        [UseDbContext(typeof(ApplicationDbContext))]
        [UseProjection]
        public IQueryable<Administrator> GetAdministrators([ScopedService] ApplicationDbContext db) => db.Administrators;

        [UseDbContext(typeof(ApplicationDbContext))]
        [UseProjection]
        public IQueryable<Client> GetClients([ScopedService] ApplicationDbContext db) => db.Clients;

        [UseDbContext(typeof(ApplicationDbContext))]
        [UseProjection]
        public IQueryable<Instructor> GetInstructors([ScopedService] ApplicationDbContext db) => db.Instructors;

        [UseDbContext(typeof(ApplicationDbContext))]
        [UseProjection]
        public IQueryable<Session> GetSessions([ScopedService] ApplicationDbContext db) => db.Sessions;

        [UseDbContext(typeof(ApplicationDbContext))]
        [UseProjection]
        public IQueryable<Speciality> GetSpecialities([ScopedService] ApplicationDbContext db) => db.Specialities;
    }
}
