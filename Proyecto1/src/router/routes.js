
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: 'VerSalas/:postid',
        component: () => import('pages/VerSalas.vue'),
        props: true,
      },
      { path: 'VerSalas', component: () => import('pages/VerSalas.vue') },
      { path: 'EditarSala', component: () => import('pages/EditarSala.vue') },
      { path: 'AgregarSala', component: () => import('pages/AgregarSala.vue') },
      { path: 'EstablecerHorario', component: () => import('pages/EstablecerHorario.vue') },
      { path: 'AgregarInstructor', component: () => import('pages/AgregarInstructor.vue') },
      { path: 'Registro', component: () => import('pages/Registro.vue') },
      {
        path: 'PublicarHorarioSala/:postid',
        component: () => import('pages/PublicarHorarioSala.vue'),
        props: true,
      },
      { path: 'auth', component: () => import('pages/Login.vue') },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
