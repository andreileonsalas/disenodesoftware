import {firebaseAuth} from 'boot/firebase'

export default ({router}) => {
  router.beforeEach((to, from, next) => {
    firebaseAuth.onAuthStateChanged(user => {
        if( to.path == '/Registro'){
            next()
        }else if( !user && to.path !== '/auth'){
            next('/auth')
        }else{
            //console.log('loggedIn');
            next()
        }    
    })
  })
}