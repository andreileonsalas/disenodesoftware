import firebase from "firebase/app"

import "firebase/firestore"
import "firebase/auth"

// Agregar configuración firebase:
const firebaseConfig = {
  apiKey: "AIzaSyC2AUWip4ADYge-6mvL0TlRmyzMqyx07K4",
  authDomain: "gymtec2021-fc7d2.firebaseapp.com",
  projectId: "gymtec2021-fc7d2",
  storageBucket: "gymtec2021-fc7d2.appspot.com",
  messagingSenderId: "600990627271",
  appId: "1:600990627271:web:4bd9e76ace83b7f738d5e2",
  measurementId: "G-6CRW9MCYKQ"
};

let firebaseApp = firebase.initializeApp(firebaseConfig)
let firebaseAuth = firebaseApp.auth()
let db = firebase.firestore();
let dba = firebase.firestore();
export { firebaseAuth, db }
export { firebase , dba }
