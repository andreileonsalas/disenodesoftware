import { db } from "boot/firebase";


const COLLECTION_NAME = "admins";

export default class Admin {

  /**
   * @param {string} name
   * @param {string} email
   */
  constructor(name, email) {
    this.name = name;
    this.email = email;
  }

  collect() {
    return {
      name: this.name,
      email: this.email
    }
  }

  static async list() {
    const response = await db.collection(COLLECTION_NAME).get();
    const data = [];
    response.forEach(doc => data.push({
      id: doc.id,
      ...doc.data()
    }));
    return data;
  }

  static async findById(id) {
    const doc = await db.collection(COLLECTION_NAME).doc(id).get();
    return !doc.exists ? null : ({
      id: doc.id,
      ...doc.data()
    });
  }

  async save() {
    const response = await db.collection(COLLECTION_NAME).add(this.collect());
    this.id = response.id;
  }

  update() {
    return db.collection(COLLECTION_NAME).doc(this.id).update(this.collect());
  }

  delete() {
    return db.collection(COLLECTION_NAME).doc(this.id).delete();
  }
}
