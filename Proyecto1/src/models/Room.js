import { db } from "boot/firebase";
import Admin from "./Admin";


const COLLECTION_NAME = "rooms";

export default class Room {

  /**
   * @param {string} name
   * @param {string} max_capacity
   * @param {string} in_use_capacity
   * @param {string[]} specialties
   * @param {Admin} admin
   */
  constructor(name, max_capacity, in_use_capacity, specialties, admin) {
    this.name = name;
    this.max_capacity = max_capacity;
    this.in_use_capacity = in_use_capacity;
    this.specialties = specialties;
    console.log(admin)
    this.adminId = admin.id;
  }

  collect() {
    return {
      name: this.name,
      max_capacity: this.max_capacity,
      in_use_capacity: this.in_use_capacity,
      specialties: this.specialties,
      adminId: this.adminId
    }
  }

  static async list() {
    const response = await db.collection(COLLECTION_NAME).get();
    const data = [];
    response.forEach(doc => data.push({
      id: doc.id,
      ...doc.data()
    }));
    return data;
  }

  static async findById(id) {
    const doc = await db.collection(COLLECTION_NAME).doc(id).get();
    return !doc.exists ? null : ({
      id: doc.id,
      ...doc.data()
    });
  }

  async save() {
    const response = await db.collection(COLLECTION_NAME).add(this.collect());
    this.id = response.id;
  }

  update() {
    return db.collection(COLLECTION_NAME).doc(this.id).update(this.collect());
  }

  delete() {
    return db.collection(COLLECTION_NAME).doc(this.id).delete();
  }
}
